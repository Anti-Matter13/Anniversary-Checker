/**
 * Sends emails with data from the current spreadsheet.
 */
function sendEmails() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var startRow = 4; // First row of data to process
  var numRows = 1; // Number of rows to process
  // Fetch the range of cells needed
  var dataRange = sheet.getRange(startRow, 4, numRows, 10);
  // Fetch values for each row in the Range.
  var data = dataRange.getValues();
  for (i in data) {
    var row = data[i];
    var emailAddress = row[0]; // First column
    var message = row[4]; // Second column
    var subject = 'Anniversary Reminder';
    MailApp.sendEmail(emailAddress, subject, message);
    function createAllDayEventSeries() { // Creates an event series for Anniversary
      CalendarApp.createAllDayEventSeries(subject, message);('Anniversary Reminder');
        new Date=(message),CalendarApp.newRecurrence().addYearlyRule();
    //{guests: 'hr@three6five.com'});
//Logger.log('Event Series ID: ' + eventSeries.getId());
    }
  }
}